# Credits

## Development Lead

* [Adrien Delsalle](https://gitlab.com/adriendelsalle)
* [Etienne Lac](https://gitlab.com/etienne.lac) (<etienne.lac@safrangroup.com>)

## Contributors

* [Duc Trung Lê](https://gitlab.com/ductrungle)
* Sébastien Raymond (<sebastien@ssg-aero.com>)
* Stéphan Aubin (<stephan.aubin@ssg-aero.com>)
