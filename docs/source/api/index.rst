.. _api:

API reference
=============

This section provides the complete public API reference for Pyoccad, auto-generated from the docstrings in
the project source code.

The full list of sections for all the modules in Pyoccad is accessible from the sidebar to the left.

Create
------

The pyoccad.create module aims to easily create geometrical entities, transformations, etc.


.. toctree::
    :maxdepth: 1
    :hidden:
    :glob:

    reference/*
