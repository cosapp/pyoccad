.. _user_guide:

User guide
==========

The User Guide covers the main usage of Pyoccad

.. toctree::
    :maxdepth: 3
    :hidden:

    notebooks/create/Bezier
    notebooks/create/BSpline
    notebooks/transform/BooleanOperation
    notebooks/transform/Extrude
    notebooks/transform/Sweep
